The smartlinebreakconverter will selectively apply the line break converter
filter (in the core filter module) based on whether it thinks it's
necessary.  This in particular resolves an issue we have with users
copying/pasting content into TinyMCE and having extraneous line breaks
hidden in the content.  Basically, if it appears the content has line breaks
in it coded with HTML (as from a WYSIWYG editor), then it will not change
the content.  If the content appears to be just text though, the core filter
line break converter will be called on the content to break paragraphs
appropriately.

Installation
------------

1. Decompress the smartlinebreakconverter file to the Drupal modules/ directory. Drupal should
   automatically detect it.

2. Enable the module in "administer" > "modules".

3. Go to "administer" > "input formats" and enable and configure the module
   for the desired input format(s).
   
   

Author
------

Neil Schelly <neil.schelly@oasis-open.org>

